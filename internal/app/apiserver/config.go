package apiserver

// Config ...
type Config struct {
	BindAddres  string `toml:"bind_addres"`
	LogLevel    string `toml:"log_level"`
	DatabaseURL string `toml:"database_url"`
	SessionKey  string `tom:"session_key"`
}

// NewConfig ...
func NewConfig() *Config {
	return &Config{
		BindAddres: ":8000",
		LogLevel:   "debug",
	}
}
