.PHONY: build
build:
	go build -v ./cmd/apiserver

migrate:
	migrate -path migrations -database "postgres://postgres:45685255@/restapi_dev?sslmode=disable" up

migrate-test:
	migrate -path migrations -database "postgres://postgres:45685255@/restapi_test?sslmode=disable" up

.DEFAULT_GOAL := build